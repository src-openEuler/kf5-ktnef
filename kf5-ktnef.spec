%undefine __cmake_in_source_build
%global framework ktnef

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf5-%{framework}
Version:        23.08.5
Release:        2
Summary:        The KTNef Library
License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global revision %(echo %{version} | cut -d. -f3)
%if %{revision} >= 50
%global stable unstable
%else
%global stable stable
%endif
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{framework}-%{version}.tar.xz

BuildRequires:  make
BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-rpm-macros
BuildRequires:  kf5-kdelibs4support-devel

#global majmin_ver %(echo %{version} | cut -d. -f1,2)
%global majmin_ver %{version}

BuildRequires:  kf5-kcalendarcore-devel
BuildRequires:  kf5-kcalendarutils-devel >= %{majmin_ver}
BuildRequires:  kf5-kcontacts-devel >= %{majmin_ver}
BuildRequires:  qt5-qtbase-devel

%if 0%{?tests}
BuildRequires:  dbus-x11
BuildRequires:  xorg-x11-server-Xvfb
%endif

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       cmake(KF5CalendarCore)
Requires:       kf5-kcalendarcore-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{version} -p1

%build
%{cmake_kf5} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name --with-html

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 10" -C %{_target_platform} ||:
%endif

%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_datadir}/qlogging-categories5/*%{framework}.*
%{_kf5_libdir}/libKPim5Tnef.so.*

%files devel
%{_includedir}/KPim5/KTNEF/
%{_kf5_libdir}/libKPim5Tnef.so
%{_kf5_libdir}/cmake/KPim5Tnef/
%{_kf5_archdatadir}/mkspecs/modules/qt_KTNef.pri

%changelog
* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 23.08.5-2
- adapt to the new CMake macros to fix build failure

* Mon Mar 18 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.4-1
- update verison to 23.08.4

* Thu Aug 17 2023 peijiankang <peijiankang@kylinos.cn> - 23.04.3-1
- 23.04.3

* Mon Jun 12 2023 misaka00251 <liuxin@iscas.ac.cn> - 22.12.0-1
- Init package
